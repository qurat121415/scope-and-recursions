# Recursion occurs when a function calls itself.
def greet():
    print("hello")
    greet()  # function calling itself
greet()


# you can get the limit of recursion by printing.......
import sys
print(sys.getrecursionlimit())  # --> 1000

#  you can increase recursion limit
import sys
sys.setrecursionlimit(2000)
print(sys.getrecursionlimit())
