# ######## scope #########
# coding area where a particular Python variable is accessible
# ######## Local Variable #########

# a variable within a function, its scope lies within the function only
# Local variables cannot be accessed outside the function

def greet():
    a = 2
    b = 10
    print("sum is: ", a + b)
greet()
# print(a)  # ERROR because "a" is outside the function


# ######## global Variable #########

# can be accessed anywhere within a program

c = 20
def sum():
    d = 5
    print("sum is: ", c + d)
sum()
print(c)  # global variable
# print(d) # ERROR local variable


# ######## global keyword #########

# global keyword allows us to modify the global variable inside a function.
a = 10
def sum():
    global a
    a = a + 10
    b = 5
    print(a + b)
sum()


# ######## nested function and it's scope #########

# each function and sub_function stores its variables in its separate workspace.
# A nested function also has its own workspace.

def qurat():
    q = 10
    def huma():
        h = 5
        print(h)
    huma()
    print(q)
    # print(h)  # ERROR_out of scope
qurat()

# practice 1
def qurat():
    x = 10
    def huma():
        global x
        x = 15
    print("huma", x)  # it will give digit '10' instead of '15'
    huma()
    print("qurat", x)
qurat()

# practice 2
def qurat():
    x = 10
    def huma():
        global x  # not compulsory
        x = 16
        print("huma", x)  # --> 16
    huma()
    print("qurat", x)  # --> 10
qurat()

# practice 3, infinite output (recursion)
def qurat():
    x = 25
    def huma():
        x = 10
        print('huma', x)
        huma()
    huma()
    print('qurat', x)
qurat()

# practice 4, recursion error
def qurat():
    x = 29
    def huma():
        x = 2
        print('huma', x)
    huma()
    print('qurat'' x')
    qurat()  # recursion error
qurat()
